package com.gitee.easyopen.register;

import java.lang.annotation.Annotation;

/**
 * @author tanghc
 */
public interface ValidationAnnotationBuilder<T extends Annotation> {
    ValidationAnnotationDefinition build(T jsr303Annotation);
}
