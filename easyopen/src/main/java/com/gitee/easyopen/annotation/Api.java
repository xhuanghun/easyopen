package com.gitee.easyopen.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 作用在service类的方法上，service类被@ApiService标记
 * @author tanghc
 *
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface Api {

    /**
     * 接口名，建议命名规则: <strong>业务.模块.名字.动词</strong>。如：<br/>
     * 支付业务订单状态修改：pay.order.status.update<br/>
     * 充值业务用户余额充值：charge.user.money.recharge<br/>
     * 充值业务用户余额查询：charge.user.money.search<br/>
     * 
     * @return
     */
    String name();

    /**
     * 接口版本号，默认"",建议命名规则：x.y，如1.0，1.1
     * 
     * @return
     */
    String version() default "";

    /**
     * 忽略验证签名,默认false.为true接口不执行验签操作,但其它验证会执行.
     * @return
     */
    boolean ignoreSign() default false;
    
    /**
     * 忽略所有验证,默认false.为true接口都不执行任何验证操作.
     * @return
     */
    boolean ignoreValidate() default false;
    
    /**
     * 是否对返回结果进行包装,如果设置成false,则直接返回业务方法结果.
     */
    boolean wrapResult() default true;

    /**
     * 设置true，不会输出json到客户端，需要调用 {@link com.gitee.easyopen.ApiContext#getResponse()} 手动返回结果
     * @return
     */
    boolean noReturn() default false;
}
