package com.gitee.easyopen.support;

import com.gitee.easyopen.Result;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ResponseHandler {
    Result caugthException(Throwable e);

    void responseResult(HttpServletRequest request, HttpServletResponse response, Object result);
}
