package com.gitee.easyopen.permission;

import java.util.List;

import com.gitee.easyopen.ManagerInitializer;

public interface PermissionManager extends ManagerInitializer {

    /**
     * 获取客户端拥有的接口
     * 
     * @param appKey
     * @return
     */
    List<String> listClientApi(String appKey);

    boolean canVisit(String appKey,String name,String version);
    
    void loadPermissionConfig();
    
    void loadPermissionCache(String configJson);
}
