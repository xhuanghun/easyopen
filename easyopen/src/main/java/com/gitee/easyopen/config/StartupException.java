package com.gitee.easyopen.config;

/**
 * @author tanghc
 */
public class StartupException extends RuntimeException {
    public StartupException(Throwable cause) {
        super(cause);
    }
}
