package com.gitee.easyopen.doc.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.gitee.easyopen.doc.DataType;

@Documented
@Retention(RUNTIME)
@Target({FIELD, PARAMETER})
public @interface ApiDocField {
    /**
     * 字段描述
     */
    String description() default "";

    /**
     * 字段名
     */
    String name() default "";

    /**
     * 数据类型，int string float
     *
     * @return
     */
    DataType dataType() default DataType.UNKNOW;

    /**
     * 是否必填
     */
    boolean required() default false;

    /**
     * 示例值
     */
    String example() default "";

    /**
     * 指定文档类
     *
     * @return
     */
    Class<?> beanClass() default Void.class;

    /**
     * 数组元素class类型
     *
     * @return
     */
    Class<?> elementClass() default Void.class;
}
