package com.gitee.easyopen.doc;

public interface Orderable {
    String getOrderName();
    int getOrder();
}
