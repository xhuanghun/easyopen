package com.gitee.easyopen.doc.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface ApiDoc {

    /**
     * 文档模块名
     * 
     * @return
     */
    String value();
    
    /**
     * 指定模块显示顺序，值越小越靠前
     * @return 值
     */
    int order() default Integer.MAX_VALUE;
}
